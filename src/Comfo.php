<?php

namespace Framework\RevoCopCli;

class Comfo {

    protected $values;

    public function __construct($values)
    {
        $this->values = array_values($values);
    }

    public function comfortify(Command $command)
    {
        $arguments = $command->getArguments();
        $flags = $command->getFlags();

        $comfoArg = [];
        $comfoFlag = [];

        if (!empty($this->values)) {
            $valCount = count($this->values);

            /**
             * Check if command Need self value
             */
            if ($command->getHasValue() === true) {
                $comfoArg['self'] = $this->values[0];
            }

            for ($i = 1; $i < $valCount; $i++) {
                $arg = $this->values[$i];

                if (in_array($arg, $arguments)) {
                    $comfoArg[$arg] = $this->values[++$i];
                } elseif (in_array($arg, $flags)) {
                    $comfoFlag[$arg] = true;
                }
            }
        }
        
        $command->setArguments($comfoArg);
        $command->setFlags($comfoFlag);
        
        return;
    }

}
