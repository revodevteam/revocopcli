<?php
namespace Framework\RevoCopCli;

class Console
{

    protected $commandCollection;
    protected $globalArguments;

    public function __construct(array $arguments)
    {
        define('REVOCOP_DIR', __DIR__);

        $this->commandCollection = new CommandCollection();
        $this->globalArguments = $arguments;

        if (empty($this->globalArguments)) {
            die('Please specify your commands');
        } else {
            array_shift($this->globalArguments);
        }
    }

    public function execute()
    {
        $command = $this->commandCollection->match(array_shift($this->globalArguments));

        if ($command instanceof Command) {
            $action = $command->getAction();

            if (empty($action->class) || empty($action->method)) {
                
            }

            $className = $action->class;
            $methodName = $action->method;

            if (class_exists($className) === false) {
                throw new \Exception($className . ' not found !');
            }

            $comfo = new Comfo($this->globalArguments);
            $comfo->comfortify($command);

            $classObj = new $className($command);
            return $classObj->$methodName();
        }

        throw new \Exception('Command "' . $command . '" Not Found !');
    }

    public function __destruct()
    {
        echo PHP_EOL . 'Make Framework Great Again' . PHP_EOL;
    }
}
