<?php
namespace Framework\RevoCopCli;

use Framework\RevoCopCli\Commands\Controller;
use Framework\RevoCopCli\Commands\Model;
use Framework\RevoCopCli\Commands\Crud;
use Framework\RevoCopCli\Commands\DB;
use Framework\RevoCopCli\Commands\Install;
use Framework\RevoCopCli\Commands\Help;

class CommandCollection
{

    protected $commands;

    public function __construct()
    {
        $this->commands = [
            (new Command('make:controller', ['class' => Controller::class, 'method' => 'create']))->hasValue(true),
            (new Command('make:model', ['class' => Model::class, 'method' => 'create']))->hasValue(true),
            (new Command('make:crud', ['class' => Crud::class, 'method' => 'create']))->hasValue(true)->setArguments(['-name']),
            (new Command('db:export', ['class' => DB::class, 'method' => 'export']))->hasValue(true)->setFlags(['--structure']),
            (new Command('db:import', ['class' => DB::class, 'method' => 'import']))->hasValue(true),
            (new Command('install', ['class' => Install::class, 'method' => 'setup']))->hasValue(false),
            (new Command('-h', ['class' => Help::class, 'method' => 'help']))->hasValue(false)
        ];
    }

    public function match(string $commandName)
    {
        if (empty($this->commands)) {
            throw new \Exception('Please insert a Command !');
        }

        foreach ($this->commands as $command) {
            if ($command->getName() === $commandName) {
                return $command;
            }
            continue;
        }

        return false;
    }

    public function getCollection()
    {
        return $this->commands;
    }
}
