<?php

namespace Framework\RevoCopCli\Commands;

use Framework\OS\Kernel\TaskManager;

/**
 * This class will execute Router
 * Controller/Method
 */
class Exec {

    protected $arguments;

    public function __construct($arguments)
    {
        $this->arguments = $arguments;
    }

    public function route()
    {
        /**
         * Prepare variables for routes
         */
        $base_url = parse_url(BASE_URL);

        $_SERVER['REQUEST_SCHEME'] = $base_url['scheme'] ?? 'http';
        $_SERVER["HTTP_HOST"] = $base_url['host'] ?? '';
        $_SERVER["REQUEST_URI"] = str_replace('//', '/', ($base_url['path'] ?? '/') . $this->arguments['exec:route'] ?? '');

        /**
         *  Load and initiate routes
         *  Will return Router instance
         */
        $router = require ROUTES;

        /**
         *  We will now start to load our task manager
         *  He is responsible for all MVC related calls
         */
        $taskManager = new TaskManager($router);
        $taskManager->loadTasks();
    }

    public function action()
    {
        $controllerAction = explode('::', $this->arguments['exec:action']);

        $controllerName = '\\Controllers\\' . $controllerAction[0].'Controller';
        $methodName = $controllerAction[1];

        return (new $controllerName())->$methodName(new \Framework\Http\Request\Request(new \Framework\Http\Session\Session));
    }

}
