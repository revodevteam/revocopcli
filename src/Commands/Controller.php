<?php
namespace Framework\RevoCopCli\Commands;

use Framework\RevoCopCli\Command;
use Inflect\Inflect;

class Controller
{

    protected $command;
    protected $moduleName;
    protected $controllerName;
    protected $controllerString;

    function __construct(Command $command)
    {
        $this->command = $command;
        $this->moduleName = $this->prepareControllerName($this->command->argument('self'));
        $this->controllerName = $this->moduleName . 'Controller';
    }

    protected function prepareControllerName($string)
    {
        return ucfirst(Inflect::singularize(str_replace(' ', null, ucwords(str_replace('_', ' ', $string)))));
    }

    public function create()
    {
        echo 'Creating new Controller : ' . $this->controllerName . PHP_EOL . PHP_EOL;
        $this->prepareControllerString();
        $this->writeController();
    }

    protected function prepareControllerString()
    {
        $this->controllerString = '<?php
namespace App\\Controllers;

use App\\Models\\' . $this->moduleName . 'Model;
use Framework\\Controller\\Controller;

class ' . $this->controllerName . ' extends Controller
{
    /**
     *  @var ' . $this->moduleName . 'Model
     */
    protected $model;
    
    public function __construct(' . $this->moduleName . 'Model $model)
    {
      $this->model = $model;
    }
}';
    }

    protected function writeController()
    {
        $ControllerPath = APP_DIR . DS . 'Controllers' . DS . $this->controllerName . '.php';

        if (is_dir(dirname($ControllerPath)) === false) {
            mkdir(dirname($ControllerPath), 0755, true);
        }

        return file_put_contents($ControllerPath, $this->controllerString);
    }
}
