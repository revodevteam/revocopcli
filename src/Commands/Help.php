<?php
namespace Framework\RevoCopCli\Commands;

use Framework\RevoCopCli\Command;
use Framework\RevoCopCli\CommandCollection;

class Help
{

    protected $command;

    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function help()
    {
        $commandCollection = new CommandCollection();
        $commands = $commandCollection->getCollection();

        echo 'Revocop CLI parameters : ' . PHP_EOL;
        if (!empty($commands)) {
            foreach ($commands as $command) {
                echo "\t" . $command->getName() . PHP_EOL;
            }
        }
    }
}
