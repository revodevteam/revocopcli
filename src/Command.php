<?php

namespace Framework\RevoCopCli;

class Command {

    protected $name;
    protected $action;
    protected $hasValue;
    protected $arguments = [];
    protected $flags = [];

    public function __construct(string $name, array $action)
    {
        $this->name = $name;
        $this->action = $action;
    }

    public function setArguments(array $arguments = [])
    {
        $this->arguments = $arguments;
        return $this;
    }

    public function setFlags(array $flags = [])
    {
        $this->flags = $flags;
        return $this;
    }

    public function hasValue(bool $hasValue)
    {
        $this->hasValue = $hasValue;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAction()
    {
        return (object) $this->action;
    }

    public function getHasValue()
    {
        return $this->hasValue;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    public function getFlags()
    {
        return $this->flags;
    }

    public function hasArgument(string $argument)
    {
        return isset($this->arguments[$argument]);
    }

    public function argument(string $argument)
    {
        if (isset($this->arguments[$argument])) {
            return $this->arguments[$argument];
        }

        return false;
    }

    public function isFlagged(string $flag)
    {
        if (isset($this->flags[$flag])) {
            return $this->flags[$flag];
        }

        return false;
    }

}
